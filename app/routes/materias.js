const express = require('express')
const router = express.Router()
const materiaController = require('../controllers/materiaController')
const auth = require('../middleware/auth')

// router.use(auth.auth)

router.get('/', (req, res) => {
  console.log('ruta de materias')
  materiaController.index(req, res)
})

router.get('/privada', auth.auth, (req, res) => {
  materiaController.index(req, res)
})

router.get('/:id', (req, res) => {
  console.log('estas en show')
  materiaController.show(req, res)
})

router.post('/', (req, res) => {
  console.log('estas en create')
  materiaController.create(req, res)
})

router.put('/:id', (req, res) => {
  console.log('estas en create')
  materiaController.update(req, res)
})

router.delete('/:id', (req, res) => {
  console.log('estas en borrar')
  materiaController.remove(req, res)
})

module.exports = router
