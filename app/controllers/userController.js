const User = require('../models/User')
const servicejwt = require('../services/servicejwt')
const register = (req, res) => {
  const user = new User({
    codigo: req.body.codigo,
    nombre: req.body.nombre,
    password: req.body.password
  })

  user.save(err => {
    if (err) res.status(500).send({ message: `Error al crear usuario: ${err}` })
    return res.status(200).send({ token: servicejwt.createToken(user) })
  })
}

module.exports = {
  register
}
