const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra API!' })
})

router.use('/cervezas', routerCervezas)

module.exports = router
