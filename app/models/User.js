const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  codigo: {
    type: String,
    required: true,
    unique: true
  },
  nombre: String,
  password: {
    type: String,
    select: false
  }
})

const User = mongoose.model('User', userSchema)

module.exports = User
